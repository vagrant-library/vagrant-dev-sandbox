#!/bin/bash

set -o errexit

#
# Upgrade the system base
#
function upgradeTheSystem {
	# Install software base
	# Install build essential
	add-apt-repository -y ppa:ubuntu-toolchain-r/test
  apt-get update
		# Upgrade the system
	apt-get upgrade -y
}

#
# Install the software base
#
function installSoftwareBase {
	apt-get install -y devscripts debhelper build-essential dh-make \
		network-manager unzip curl wget mlocate software-properties-common	\
		dos2unix python
	apt-get install -y git
	git config --global user.name "Vagrant Machine"
	git config --global user.email "vagrant-machine@domain.com"
	# Configure git. Disable SSL Check
	cat << EOF | sudo tee /etc/gitconfig
[http]
  sslVerify = false
EOF
}

echo " ========================================"
echo "  Provisioning the Server"
echo " ========================================"

upgradeTheSystem
installSoftwareBase

echo "Server Successfully Provisioned"
