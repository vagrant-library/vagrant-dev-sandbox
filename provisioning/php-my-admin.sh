#!/bin/bash

set -o errexit

DBPASSWD="root"

function installPHPMyAdmin {
  debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
  debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $DBPASSWD"
  debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $DBPASSWD"
  debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $DBPASSWD"
  debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none"
  apt-get install -y phpmyadmin
}

function restartServices {
  # Now Restart the server
  systemctl restart php7.2-fpm
  systemctl restart nginx
}

echo " ========================================"
echo "  Provisioning PHPMyAdmin"
echo " ========================================"

installPHPMyAdmin
restartServices

echo "PHPMyAdmin Successfully Provisioned"
