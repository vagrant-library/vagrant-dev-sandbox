#!/bin/bash

set -o errexit

function installJhipsterGenerator {
  sudo npm install -g generator-jhipster  
}

echo " ========================================"
echo "  Installing JHipster generator the Server"
echo " ========================================"

installJhipsterGenerator

echo "JHipster generator successfully installed"
