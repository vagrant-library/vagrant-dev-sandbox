#!/bin/bash

set -o errexit

function installNodeJS {
  curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -; sudo apt-get install -y nodejs
}

function installYarn {
  sudo npm install -g yarn  
  su -c "yarn config set prefix /home/jhipster/.yarn-global" jhipster
}

function installNpm {
  sudo npm install -g npm
  sudo npm update -g npm
}

function installYo {
  sudo npm install -g yo
  sudo npm install -g generator-webapp
  sudo npm install -g generator-angular
}

function installBower {
  sudo npm install -g bower
}

function installGrunt {
  sudo npm install -g coffee-script
  sudo npm install -g grunt-cli
}

function installGulp {

  sudo npm install -g gulp
}

function installWebPack {
  sudo npm install -g webpack
  sudo npm install -g webpack-cli
}

function installProtractor {
  sudo npm install -g protractor
}

echo " ========================================"
echo "  Provisioning NodeJS and company"
echo " ========================================"

installNodeJS
installNpm
installYo
installBower
installGrunt
installGulp
installWebPack
installProtractor

echo "NodeJS Successfully Provisioned"
