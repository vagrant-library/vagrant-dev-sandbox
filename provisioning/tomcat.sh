#!/bin/bash

function installTomcat {
  # Define user and groups
  groupadd tomcat
  useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat
  cp /vagrant/contrib/tomcat/apache-tomcat-8.5.29.tar.gz /home/vagrant/

  mkdir /opt/tomcat
  tar xzvf /home/vagrant/apache-tomcat-8.5.29.tar.gz -C /opt/tomcat --strip-components=1
  rm /home/vagrant/apache-tomcat-8.5.29.tar.gz

  # Config server
  cp /vagrant/config/tomcat/conf/tomcat-users.xml /opt/tomcat/conf/tomcat-users.xml
  cp /vagrant/config/tomcat/webapps/manager/META-INF/context.xml /opt/tomcat/webapps/manager/META-INF/context.xml
  cp /vagrant/config/tomcat/webapps/host-manager/META-INF/context.xml /opt/tomcat/webapps/host-manager/META-INF/context.xml
}

function updatePermissions {
    # Update Permissions
    chgrp -R tomcat /opt/tomcat
    chmod -R g+r /opt/tomcat/conf
    chmod g+x /opt/tomcat/conf
    chown -R tomcat /opt/tomcat/webapps/ /opt/tomcat/work/ /opt/tomcat/temp/ /opt/tomcat/logs/
}

function setupTomcatInit {
    # Setup the tomcat init
    cp /vagrant/config/tomcat/tomcat.service /etc/systemd/system/tomcat.service
    systemctl daemon-reload
    systemctl start tomcat
    systemctl enable tomcat
}

echo " ========================================"
echo "  Setup Tomcat"
echo " ========================================"

installTomcat
updatePermissions
setupTomcatInit

echo "Tomcat setup complete"
