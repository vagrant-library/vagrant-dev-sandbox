#!/bin/bash

JAVA_MYSQL_CONNECTOR_VERSION=5.1.40
JAVA_MYSQL_CONNECTOR_JAR=mysql-connector-java-${JAVA_MYSQL_CONNECTOR_VERSION}.jar
JAVA_MYSQL_CONNECTOR_DOWNLOAD=http://central.maven.org/maven2/mysql/mysql-connector-java/${JAVA_MYSQL_CONNECTOR_VERSION}/mysql-connector-java-${JAVA_MYSQL_CONNECTOR_VERSION}.jar

function installJava {
  echo "Provisioning Java"
  echo oracle-java11-installer shared/accepted-oracle-license-v1-2 select true | sudo /usr/bin/debconf-set-selections
  sudo add-apt-repository ppa:linuxuprising/java -y
  sudo apt-get update
  sudo apt-get install -y oracle-java11-installer
  sudo apt-get install -y oracle-java11-set-default
}

function setupEnvVars {
  echo "Setting JAVA_HOME"
	JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:/bin/java::")
	ln -s ${JAVA_HOME} /usr/local/java
  echo "export JAVA_HOME=/usr/local/java" >> /etc/profile.d/java.sh
	echo "export PATH=/usr/local/java/bin:\${PATH}" >> /etc/profile.d/java.sh
	source /etc/profile.d/java.sh
}

function installMaven {
  echo "Provisioning Maven"
  # Get the package
  cp /vagrant/contrib/maven/apache-maven-3.3.9-bin.tar.gz /home/vagrant/
  # Unzip the package
  sudo mkdir -p /opt/maven
  sudo tar -zxvf /home/vagrant/apache-maven-3.3.9-bin.tar.gz -C /opt/maven --strip-components=1
  # Set the environment variables
  sudo sh -c "echo export M2_HOME=/opt/maven >> /etc/environment"
  sudo sh -c "echo export PATH=/opt/maven/bin:${PATH} >> /etc/environment"
  # Clean the bin file
  rm /home/vagrant/apache-maven-3.3.9-bin.tar.gz
}

function installGradle {
  echo "Provisioning Maven"
  # Get the package
  cp /vagrant/contrib/gradle/gradle-5.4.1-all.zip /home/vagrant/
  # Unzip the package
  sudo unzip -d /opt/gradle /home/vagrant/gradle-*.zip
  # Set the environment variables
  sudo sh -c "echo export GRADLE_HOME=/opt/gradle/gradle-5.4.1 >> /etc/environment"
  sudo sh -c "echo export PATH=/opt/gradle/gradle-5.4.1/bin:${PATH} >> /etc/environment"
  # Clean the bin file
  rm /home/vagrant/gradle-5.4.1-all.zip
}

echo " ========================================"
echo "  Setup Java"
echo " ========================================"

installJava
setupEnvVars
installMaven
installGradle

echo "Java setup complete"
