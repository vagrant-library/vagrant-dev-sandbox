#!/bin/bash

set -o errexit

DBPASSWD="root"

function installPHP {
  apt-get install -y php-fpm php-mysql php-curl php-xdebug
}

function restartServices {
  # Now Restart the server
  systemctl restart php7.2-fpm
}

echo " ========================================"
echo "  Provisioning PHP fpm"
echo " ========================================"

installPHP
restartServices

echo "PHP fpm Successfully Provisioned"
