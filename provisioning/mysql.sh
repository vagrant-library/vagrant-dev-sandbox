#!/bin/bash

set -o errexit

MYSQL_ROOT_USER="root"
MYSQL_ROOT_PASSWORD="root"

function installMySQL {
  debconf-set-selections <<< "mysql-server mysql-server/root_password password ${MYSQL_ROOT_PASSWORD}"
  debconf-set-selections <<< "mysql-server mysql-server/root_password_again password ${MYSQL_ROOT_PASSWORD}"
  apt-get -y install mysql-server
}

function secureMySQLInstallation {
  mysql -u root -p${MYSQL_ROOT_PASSWORD} -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION; FLUSH PRIVILEGES; SET GLOBAL max_connect_errors=10000;"
}

function configureMySQL {
  sed -i 's/^bind-address.*/bind-address = 0.0.0.0/g' /etc/mysql/mysql.conf.d/mysqld.cnf
}

function initDatabase {
  mysql -u root -p${MYSQL_ROOT_PASSWORD} -e "CREATE DATABASE sandbox"
  mysql -u root -p${MYSQL_ROOT_PASSWORD} -D sandbox < /vagrant/config/mysql/schema.sql
  mysql -u root -p${MYSQL_ROOT_PASSWORD} -D sandbox < /vagrant/config/mysql/data.sql
}

function restartMySQL {
  systemctl restart mysql
}

echo " ========================================"
echo "  Provisioning MySQL"
echo " ========================================"

installMySQL
secureMySQLInstallation
configureMySQL
initDatabase
restartMySQL

echo "MySQL Successfully Provisioned"
