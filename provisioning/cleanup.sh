#!/bin/bash

set -o errexit

function cleanUpTheSystem {
  sudo apt-get autoclean
  sudo apt-get clean
  sudo apt autoremove
}

function adjustRightPermissions {
  sudo chown -R vagrant:vagrant /home/vagrant
}

function showFinalMessage {
  # YEAH!!!!
  echo " ======================================================================"
  echo " If all went well, you should ready to get started with Dev Sandbox!"
  echo ""
  echo " You can login to this VM with:"
  echo "   vagrant ssh"
  echo ""
  echo " The IP for this VM is: $(/sbin/ip -o -4 addr list enp0s3 | awk '{print $4}' | cut -d/ -f1)"
  echo ""
  echo " Run 'JHipster' in the VM to start creating your first example app- Enjoy!!!"
  echo " ======================================================================"
}

echo " ========================================"
echo "  Cleaning up the Server"
echo " ========================================"

#cleanUpTheSystem
#adjustRightPermissions
showFinalMessage

echo "Clean up successfully"
