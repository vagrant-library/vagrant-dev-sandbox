#!/bin/bash

function installNginx {
  # install NGINX
  apt-get install -y nginx ssl-cert apache2-utils

  # update the default vhost
  cat /vagrant/config/nginx/nginx.conf > /etc/nginx/nginx.conf

  # Link the sites enabled with the vagrant folder configuration.
  rm -Rf /etc/nginx/sites-enabled
  ln -fs /vagrant/config/nginx/sites-enabled /etc/nginx/

  # Generate certificate
  make-ssl-cert generate-default-snakeoil --force-overwrite

	# restart nginx so it can pick up the new configuration
	service nginx restart
}

echo " ========================================"
echo "  Setup Nginx"
echo " ========================================"

installNginx

echo "Nginx setup complete"
