SELECT  estado_factura,
		importe_factura,
		nombre_grupo,
		num_factura,
		incremento_factura,
		fecha
FROM factura
WHERE fecha BETWEEN DATE_SUB(TIMESTAMP(:sql_last_value), INTERVAL 1 HOUR) AND :sql_last_value